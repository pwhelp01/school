%
% Programminging Language Paradigms (UG) Coursework 2015/2016
% Prolog Assignment
% Submitted by: Pete Whelpton (pwhelp01 / 12828513)
% Lecturers: Keith Mannock / Trevor Fenner
% Due date: 1st April 2016
%

% Revision History
% 22/11/2015 - Initial Commit.  No syntax errors, but churns for 5+ minutes and can't find a solution :(
% 19/03/2016 - *FINALLY!* got it to return results.  Problem was a type in rule 1 (it was missing 'gross' completely from one of the rules), and the fact that I used the same variable 'F' for all instances of a female (so Prolog was trying to fit the same person in all three places...).  Replaced 'F' with 'F1', 'F2', 'F3'.  It works!
% Returning two results though - need to check if there is an error, or if this is what is supposed to happen.
% Also removed the teacher() facts (as they are actually superfluous to the problem) and the male() facts for the same reason.


% Solution shamelessly based on the "It's a tie" example from class %

 
% Facts %
% Gender facts
female(gross).
female(appleton).
female(parnell).

% Subject facts
subject(maths).
subject(science).
subject(history).
subject(pe).
subject(english).

% County facts
county(suffolk).
county(cornwall).
county(norfolk).
county(yorkshire).
county(cumbria).
county(hertfordshire).

% Activity facts
activity(nudistcolony).
activity(bodyboarding).
activity(swimming).
activity(camping).
activity(sightseeing).

 
% Main Solution %
solve :- 	% Teacher / Subject relations
			subject(KnightSubject), subject(GrossSubject), subject(McevoySubject), subject(AppletonSubject), subject(ParnellSubject),
			% Create a list of teacher/subject relations and ensure they are all different
			all_different([KnightSubject, GrossSubject, McevoySubject, AppletonSubject, ParnellSubject]),

			% Teacher / County relations
			county(KnightCounty), county(GrossCounty), county(McevoyCounty), county(AppletonCounty), county(ParnellCounty),
			% Create a list of teacher/County relations and ensure they are all different
			all_different([KnightCounty, GrossCounty, McevoyCounty, AppletonCounty, ParnellCounty]),
			
			% Teacher / Activity relations
			activity(KnightActivity), activity(GrossActivity), activity(McevoyActivity), activity(AppletonActivity), activity(ParnellActivity),
			% Create a list of teacher/Activity relations and ensure they are all different
			all_different([KnightActivity, GrossActivity, McevoyActivity, AppletonActivity, ParnellActivity]),
			
			
			% Create a list of Holidays consisting of lists for each teacher
			Holidays = 	[
							[knight, KnightSubject, KnightCounty, KnightActivity],
							[gross, GrossSubject, GrossCounty, GrossActivity],
							[mcevoy, McevoySubject, McevoyCounty, McevoyActivity],
							[appleton, AppletonSubject, AppletonCounty, AppletonActivity],
							[parnell, ParnellSubject, ParnellCounty, ParnellActivity]
						],
			
			
			/* Clues */
			% 1. Ms.  Gross teaches either maths or science.  
			(
				member([gross, maths, _, _], Holidays); 
				member([gross, science, _, _], Holidays)
			),
			
			% If Ms.  Gross is going to a nudist colony, then she is going to Suffolk; 
			(
				member([gross, _, suffolk, nudistcolony], Holidays);
			
			%otherwise she is going to Cornwall.
				(
					member([gross, _, cornwall, _], Holidays), 
					\+ member([gross, _, _, nudistcolony], Holidays)
				)
			),
			
			
			% 2. The science teacher (who is going body-boarding) is going to travel to Cornwall or Norfolk.
			(
				member([_, science, cornwall, bodyboarding], Holidays);
				member([_, science, norfolk, bodyboarding], Holidays)
			),
			
			
			% 3. Mr. McEvoy (who is the history teacher) is going to Yorkshire or Cumbria.
			(
				member([mcevoy, history, yorkshire, _], Holidays);
				member([mcevoy, history, cumbria, _], Holidays)
			),
			
			
			% 4. If the woman who is going to Hertfordshire... 
			 member([F1, _, hertfordshire, _], Holidays),
			
			
			% ...is the English teacher, then she is Ms. Appleton; 
			(
				member([appleton, english, hertfordshire, _], Holidays);
				
				% otherwise, she is Ms. Parnell 
				member([parnell, _, hertfordshire, _], Holidays)
			),
			
			% (who is going swimming).
			member([parnell, _, _, swimming], Holidays),
			
			% 5. The person who is going to Yorkshire (who isn't the PE teacher)... 
			member([_, _, yorkshire, _], Holidays),
			\+ member([_, pe, yorkshire, _], Holidays),
			 
			%...isn't the one who is going sightseeing.
			\+ member([_, _, yorkshire, sightseeing], Holidays),
			
			
			% 6. Ms. Gross isn't the woman who going camping
			member([F2, _, _, camping], Holidays),
			\+ member([gross, _, _, camping], Holidays),
			
			
			% 7. One woman is going to a nudist colony on her holiday.
			member([F3, _, _, nudistcolony], Holidays),
			
			
			% Condition where teacher has to be a woman
			female(F1),
			female(F2),
			female(F3),
			
			% Display results %
			tell(knight, KnightSubject, KnightCounty, KnightActivity),
			tell(gross, GrossSubject, GrossCounty, GrossActivity),
			tell(mcevoy, McevoySubject, McevoyCounty, McevoyActivity),
			tell(appleton, AppletonSubject, AppletonCounty, AppletonActivity),
			tell(parnell, ParnellSubject, ParnellCounty, ParnellActivity).

			
% "Borrowed" from the example
% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

% "Borrowed" from the example
tell(T, S, C, A) :-
					write(T), write(' who teaches '), write(S), write(' is going to '), write(C), write(' to go '), write(A), nl.
			
			